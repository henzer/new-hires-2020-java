package com;

import java.util.HashMap;
import java.util.Map;

/*
*
* Add the necessary unit test to validate the following methods
 */
public class Operators {

    public int[] order(final int[] unordered) {
        final int count = unordered.length;

        for (int i = 0; i < unordered.length; i++) {
            for (int j = i + 1; j < count; j++) {
                if (unordered[i] > unordered[j]) {
                    int temp = unordered[i];
                    unordered[i] = unordered[j];
                    unordered[j] = temp;
                }
            }
        }
        return unordered;
    }

    public int addition(final int num1, final int num2) {
        return num1 + num2;
    }

    public int substraction(final int num1, final int num2) {
        return num1 - num2;
    }

    public int multiplication(final int num1, final int num2) {
        return num1 * num2;
    }

    public int division(final int num1, final int num2) {
        return num1 / num2;
    }

    public boolean eval(final int num1, final int num2) {
        return num1 > num2;
    }

    public String getTemplateName(final String type) {
        return "channel".equals(type) ? "option1" : "option2";
    }

    public Map<String, String> getModel(final boolean fill) {
        final Map<String, String> model = new HashMap<>();

        if (fill) {
            model.put("key1", "value1");
            model.put("key2", "value2");
            model.put("key3", "value3");
            model.put("key4", "value4");
            model.put("key5", "value5");
        }

        return model;
    }
}
