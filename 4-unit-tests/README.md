# Unit tests
This is the best way to ensure that the code does what it is supposed to do, and to find bugs at an early stage. 

## Instructions

Add unit tests to the `unittests` project.
