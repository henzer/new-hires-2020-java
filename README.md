# New hires JAVA test
This repository was created with the purpose of evaluating your skill as a JAVA developer.
1. Theory
2. Code Review
3. Hands-on coding
4. Unit tests

## Instructions:
1. Clone this repository: `git clone https://bitbucket.org/henzer/new-hires-2020-java.git`
2. Create a new branch from `master` and name it as `<firstname-lastname>`. Example: `git checkout -b john-doe`
3. In the new branch solve the modules: `1-theory`, `3-hands-on`, `4-unit-tests`.
4. Once modules `1-theory`, `3-hands-on`, `4-unit-tests` are finished, create a pull request pointing to the branch named `code-review-destination`.
5. Resolve `2-code-review` directly in the pull request.

### Consider: 
* You must share your screen during the test.
* You can search any information you want on the Internet.
* If you have doubts about the test feel free to ask the interviewer(s).
