package com.rodpineda.oca.test;

/*
    Functional requirement:

    As an recruiter I wan't to generate a list of questions, based on a list of categories, that I'm going to ask to the aspirant and write them on a file.
    The file must contain the name of the aspirant, the name of the recruiter and the list of questions to be asked.
 */

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class Person {
    public String name;

    public Person(String name) {
        this.name = name;
    }
}

class Recruiter extends Person {
    public Recruiter(final String name) {
        super(name);
    }
}

class Aspirant extends Person {
    public Aspirant(final String name) {
        super(name);
    }
}

public class Interview {

    public Aspirant a;
    public Recruiter b;
    public String message;

    Interview(Aspirant a, Recruiter b) {
        this.a = a;
        this.b = b;
    }

    // Generates a question for the interview based on a category
    public String gq(String category) {

        final String categoryReceivedAsParameter = category;

        if (categoryReceivedAsParameter.equals("personality")) {
            return "How was your relationship with your last work partners?";

        } else if (categoryReceivedAsParameter.equals("code")) {
            return "Does Java runs on any computer with JVM?";

        } else if (categoryReceivedAsParameter.equals("last job")) {
            return "Please, tell me about your last job.";

        } else if (categoryReceivedAsParameter.equals("personal aspirations")) {
            return "How do you see youself at five years from now?";

        } else if (categoryReceivedAsParameter.equals("hobbies")) {
            return "What do you do in your free time?";

        } else if (categoryReceivedAsParameter.equals("experience")) {
            return "What's your experience woking with Java?";

        } else if (categoryReceivedAsParameter.equals("solution design")) {
            return "When would you use a service proxy and why?";

        } else if (categoryReceivedAsParameter.equals("last book read") || categoryReceivedAsParameter.equals("favorite food") || categoryReceivedAsParameter.equals("pets") {
            return "Do you consider your self an easily stressed person?";
        }

        return "";
    }


    public boolean writeToFile() {
        try {
            FileWriter myWriter = new FileWriter("filename.txt");

            String questionCategories[] = {
                    "code",
                    "last job",
                    "hobbies",
                    "english level",
                    "favorite food"
            };

            List<String> questions = new ArrayList<>();

            for (String c : questionCategories) {
                String question = this.gq(c);
                questions.add(question);
            }

            // myWriter.write("Questions for " + this.a.name +"\n");

            myWriter.write("Aspirant name: " + this.a.name + "\n");
            myWriter.write("Recruiter name: " + this.b.name + "\n");
            myWriter.write("\n");
            myWriter.write("List of questions: \n");
            myWriter.write(questions.toString());
            myWriter.close();
            return true;
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return false;
        }
    }


    public static void main(String[] args) {

        Interview i = new Interview(new Aspirant("John Doe"), new Recruiter("Mike Martin"));
        i.writeToFile();

    }
}
