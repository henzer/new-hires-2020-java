# Code Review
Do you know how important the code-review process is? Sadly, many companies don't care about it. But we are the exception. 

> Code review is systematic examination … of computer source code. It is intended to find and fix mistakes overlooked in the initial development phase, improving both the overall quality of software and the developers’ skills.

## Instructions
1. Create a Pull Request from your branch to the branch called "code-review-destination".
2. On the Pull Request take a look at the `Interview.java` file. Pay special attention to the functional requirement.
3. Place comments wherever you consider the code could be improved. Some things you may want to prevent in the code are:
	1. Inconsistent design and implementation.
	2. Wrong use of variables and function names.
	3. Unneeded complexity in a function or class.
	4. Any other stuff you consider could be done in a better way.
4. Place comments to ask any questions in case something is not clear for you in the code.
5. Remember to be polite in your comments.

Remember every comment and observations will represent a point and at the end it will summarize 10 points.
We have added an extra bonus point.
